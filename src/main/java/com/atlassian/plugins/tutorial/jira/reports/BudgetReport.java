package com.atlassian.plugins.tutorial.jira.reports;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.charts.jfreechart.*;
import com.atlassian.jira.charts.ChartFactory;
import com.atlassian.jira.charts.util.ChartUtils;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.datetime.DateTimeStyle;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.customfields.option.LazyLoadedOption;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.IssueConstantImpl;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.issue.issuetype.IssueTypeImpl;
import com.atlassian.jira.issue.label.Label;
import com.atlassian.jira.issue.priority.PriorityImpl;
import com.atlassian.jira.issue.status.StatusImpl;
import com.atlassian.jira.issue.search.ReaderCache;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchQuery;
import com.atlassian.jira.issue.issuetype.IssueTypeImpl;
import com.atlassian.jira.issue.resolution.ResolutionImpl;
import com.atlassian.jira.issue.statistics.FilterStatisticsValuesGenerator;
import com.atlassian.jira.issue.statistics.StatisticsMapper;
import com.atlassian.jira.issue.statistics.StatsGroup;
import com.atlassian.jira.issue.statistics.util.OneDimensionalDocIssueHitCollector;
import com.atlassian.jira.plugin.report.impl.AbstractReport;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.ProjectImpl;
import com.atlassian.jira.project.version.VersionImpl;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.DelegatingApplicationUser;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.atlassian.jira.web.action.ProjectActionSupport;
import com.atlassian.jira.web.bean.I18nBean;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.JiraImport;
import com.atlassian.util.profiling.UtilTimerStack;
import com.atlassian.jira.bc.project.component.ProjectComponentImpl;
import com.atlassian.jira.issue.DocumentIssueImpl;
import com.atlassian.jira.project.Project;


import org.apache.log4j.Logger;
import org.apache.lucene.search.Collector;
import org.jfree.data.general.DefaultPieDataset;
import com.opensymphony.util.TextUtils;
import org.apache.commons.lang.StringUtils;
import java.util.Arrays;
import java.util.Map;
import java.util.Iterator;
import java.util.Set;
import java.util.LinkedHashSet;
import java.util.Collections;
import java.util.HashMap;

@Scanned
public class BudgetReport extends AbstractReport {
    private static final Logger log = Logger.getLogger(BudgetReport.class);
    /*
    @Override
    public boolean isExcelViewSupported() {
    return true;
    }*/
    @JiraImport
    private final SearchProvider searchProvider;
    @JiraImport
    private final SearchRequestService searchRequestService;
    @JiraImport
    private final IssueFactory issueFactory;
    @JiraImport
    private final CustomFieldManager customFieldManager;
    @JiraImport
    private final IssueIndexManager issueIndexManager;
    @JiraImport
    private final SearchService searchService;
    @JiraImport
    private final FieldVisibilityManager fieldVisibilityManager;
    @JiraImport
    private final FieldManager fieldManager;
    @JiraImport
    private final ProjectManager projectManager;
    @JiraImport
    private final ReaderCache readerCache;
    private final DateTimeFormatter formatter;
    private final Map<Object, Double> groupAcum = Collections.synchronizedMap(new HashMap());
    public BudgetReport(final SearchProvider searchProvider,
                                            final SearchRequestService searchRequestService, final IssueFactory issueFactory,
                                            final CustomFieldManager customFieldManager, final IssueIndexManager issueIndexManager,
                                            final SearchService searchService, final FieldVisibilityManager fieldVisibilityManager,
                                            final ReaderCache readerCache,
                                            final FieldManager fieldManager,
                                            final ProjectManager projectManager,
                                            @JiraImport DateTimeFormatterFactory dateTimeFormatterFactory) {
        this.searchProvider = searchProvider;
        this.searchRequestService = searchRequestService;
        this.issueFactory = issueFactory;
        this.customFieldManager = customFieldManager;
        this.issueIndexManager = issueIndexManager;
        this.searchService = searchService;
        this.fieldVisibilityManager = fieldVisibilityManager;
        this.readerCache = readerCache;
        this.fieldManager = fieldManager;
        this.projectManager = projectManager;
        this.formatter = dateTimeFormatterFactory.formatter().withStyle(DateTimeStyle.DATE).forLoggedInUser();
    }
    public String capitalizeFirstLetter(String original) {
        if (original == null || original.length() == 0) {
            return original;
        }
        return original.substring(0, 1).toUpperCase() + original.substring(1);
    }
    public StatsGroup getOptions(SearchRequest sr, ApplicationUser user, StatisticsMapper mapper, String fieldId) throws PermissionException {
        try {
            return searchMapIssueKeys(sr, user, mapper, fieldId);
        } catch (SearchException e) {
            log.error("Exception rendering " + this.getClass().getName() + ".  Exception \n" + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }     
    public StatsGroup searchMapIssueKeys(SearchRequest request, ApplicationUser searcher, StatisticsMapper mapper, String fieldId) throws SearchException {
        try {
            UtilTimerStack.push("Search Count Map");
            StatsGroup statsGroup = new StatsGroup(mapper);
            org.apache.lucene.search.IndexSearcher isrch = issueIndexManager.getIssueSearcher();                
            org.apache.lucene.index.IndexReader srch = isrch.getIndexReader();
            Collector hitCollector = new OneDimensionalDocIssueHitCollector(mapper.getDocumentConstant(), statsGroup,
            srch , issueFactory, fieldVisibilityManager, readerCache, fieldManager);     

            SearchQuery sq = SearchQuery.create((request != null) ? request.getQuery() : null, searcher);
            searchProvider.search(sq, hitCollector);                        
            Set set = statsGroup.entrySet();
            Iterator<Object> iteratorGroup = set.iterator();
            while(iteratorGroup.hasNext()) {
                Map.Entry<Object,LinkedHashSet<DocumentIssueImpl>> mentry = (Map.Entry)iteratorGroup.next();
                Object group = (Object) mentry.getKey();
                groupAcum.put(group,(double) 0);
                LinkedHashSet<DocumentIssueImpl> listIssueInComponent = mentry.getValue();         
                Iterator<DocumentIssueImpl> iteratorIssues = listIssueInComponent.iterator();
                while(iteratorIssues.hasNext()) {
                    DocumentIssueImpl issueComponent = iteratorIssues.next();
                    CustomField cf=ComponentAccessor.getCustomFieldManager().getCustomFieldObject(fieldId);
                    if (issueComponent.getCustomFieldValue(cf)!=null)
                        groupAcum.replace(group,(double) issueComponent.getCustomFieldValue(cf) + (double) groupAcum.get(group));
                    else
                        groupAcum.replace(group,0D + (double) groupAcum.get(group));                    
                }                              
            }                 
            return statsGroup;
        } finally {
            UtilTimerStack.pop("Search Count Map");
        }
    }
    
    public String generateReportHtml(ProjectActionSupport action, Map params) throws Exception {
          
        String filterId = (String) params.get("filterid");    
        if (filterId == null) {
            log.info("Single Level Group By Report run without a project selected (JRA-5042): params=" + params);
            return "<span class='errMsg'>No search filter has been selected. Please "
                    + "<a href=\"IssueNavigator.jspa?reset=Update&pid="
                    + TextUtils.htmlEncode((String) params.get("selectedProjectId"))
                    + "\">create one</a>, and re-run this report.";
        }       

        String mapperNameId = (String) params.get("groupfield");
        String mapperName = mapperNameId;
        String fieldId = (String) params.get("fieldid"); 
        CustomField fieldName=ComponentAccessor.getCustomFieldManager().getCustomFieldObject(fieldId); 
        final StatisticsMapper mapper = new FilterStatisticsValuesGenerator().getStatsMapper(mapperNameId);

        final JiraServiceContext ctx = new JiraServiceContextImpl(action.getLoggedInUser());
        final SearchRequest request = searchRequestService.getFilter(ctx, new Long(filterId));
        try {       
            DefaultPieDataset dataset = new DefaultPieDataset ();                   
            final Map <String, Object> startingParams =new HashMap<String, Object>();
            startingParams.put("action", action);
            startingParams.put("statsGroup", getOptions(request, action.getLoggedInUser(), mapper, fieldId));
            // Create Dataset      
         
            Set setGa = groupAcum.entrySet();  

            Iterator<Object> iteratorG = setGa.iterator();
            while(iteratorG.hasNext()) {          
                
                String labelChartValue="";
                String obj;
                Map.Entry<Object,Double> mentry = (Map.Entry)iteratorG.next();      
                if (mentry.getKey()!=null){
                    obj = mentry.getKey().getClass().toString();
                } else
                    obj="none";
                if (obj.equals("class com.atlassian.jira.issue.priority.PriorityImpl")) {                
                    PriorityImpl groupAcum = PriorityImpl.class.cast(mentry.getKey());
                    labelChartValue = groupAcum.getName();
                }else if (obj.equals("class com.atlassian.jira.user.DelegatingApplicationUser")) {                 
                    DelegatingApplicationUser groupAcum = DelegatingApplicationUser.class.cast(mentry.getKey());
                    labelChartValue = groupAcum.getUsername();
                }else if (obj.equals("class com.atlassian.jira.issue.issuetype.IssueTypeImpl")) {                 
                    IssueTypeImpl groupAcum = IssueTypeImpl.class.cast(mentry.getKey());
                    labelChartValue = groupAcum.getName();
                }else if (obj.equals("class com.atlassian.jira.project.ProjectImpl")) {                 
                    ProjectImpl groupAcum = ProjectImpl.class.cast(mentry.getKey());
                    labelChartValue = groupAcum.getName();
                }else if (obj.equals("class com.atlassian.jira.issue.status.StatusImpl")) {                 
                    StatusImpl groupAcum = StatusImpl.class.cast(mentry.getKey());
                    labelChartValue = groupAcum.getName();
                }else if (obj.equals("class com.atlassian.jira.issue.label.Label")) {                 
                    Label groupAcum = Label.class.cast(mentry.getKey());
                    labelChartValue = groupAcum.getLabel();
                }else if (obj.equals("class com.atlassian.jira.bc.project.component.ProjectComponentImpl")) {                 
                    ProjectComponentImpl groupAcum = ProjectComponentImpl.class.cast(mentry.getKey());
                    labelChartValue = groupAcum.getName();    
                }else if (obj.equals("class com.atlassian.jira.issue.resolution.ResolutionImpl")) {                 
                    ResolutionImpl groupAcum = ResolutionImpl.class.cast(mentry.getKey());
                    labelChartValue = groupAcum.getName();    
                }else if (obj.equals("class com.atlassian.jira.issue.customfields.option.LazyLoadedOption")) {                 
                    LazyLoadedOption groupAcum = LazyLoadedOption.class.cast(mentry.getKey());
                    labelChartValue = groupAcum.getValue();  
                    CustomField mapperId=ComponentAccessor.getCustomFieldManager().getCustomFieldObject(mapperNameId);     
                    mapperName = mapperId.getFieldName();  
                }else if (obj.equals("class com.atlassian.jira.project.version.VersionImpl")) {                 
                    VersionImpl groupAcum = VersionImpl.class.cast(mentry.getKey());
                    labelChartValue = groupAcum.getName();    
                }
                Object groupAcum = mentry.getKey();
                String componentName="";
                Double componentValue=0D;             
                componentName = (groupAcum != null) ? labelChartValue : "None";                      
                componentValue= (mentry.getValue().equals(null)) ? 0D: mentry.getValue();                
                dataset.setValue(componentName, componentValue);                                                                                              
            }   
            final I18nBean i18nBean = new I18nBean(action.getLoggedInUser());
            final ChartHelper helper = new PieChartGenerator (dataset, i18nBean).generateChart();
            helper.generateInline(ChartFactory.REPORT_IMAGE_WIDTH,ChartFactory.REPORT_IMAGE_HEIGHT);  
            String base64Image = ComponentAccessor.getComponent(ChartUtils.class).renderBase64Chart(helper.getImage(), "Digital Plan Budget Report");           
            startingParams.put("searchRequest", request);
            startingParams.put("mapperType", capitalizeFirstLetter(mapperName));             
            startingParams.put("customFieldManager", customFieldManager);
            startingParams.put("fieldVisibility", fieldVisibilityManager);
            startingParams.put("searchService", searchService);
            startingParams.put("portlet", this);
            startingParams.put("formatter", formatter);
            startingParams.put("chartDataset", dataset);
            startingParams.put("imagemap", helper.getImageMapHtml());
            startingParams.put("imageMapName", helper.getImageMapName());
            startingParams.put("width",ChartFactory.REPORT_IMAGE_WIDTH);
            startingParams.put("height",ChartFactory.REPORT_IMAGE_HEIGHT);  
            startingParams.put("acum",groupAcum);   
            startingParams.put("fieldName",fieldName.getFieldName());                      
            startingParams.put("base64Image", base64Image);                       

            return descriptor.getHtml("view", startingParams);

        } catch (PermissionException e) {
            log.error(e.getStackTrace());
            return null;
        }
    }

    public void validate(ProjectActionSupport action, Map params) {
        super.validate(action, params);
        String filterId = (String) params.get("filterid");
        if (StringUtils.isEmpty(filterId)) {
            action.addError("filterid", action.getText("report.singlelevelgroupby.filter.is.required"));
        } else {
            validateFilterId(action, filterId);
        }
    }
    private void validateFilterId(ProjectActionSupport action, String filterId) {
        try {
            JiraServiceContextImpl serviceContext = new JiraServiceContextImpl(
                    action.getLoggedInUser(), new SimpleErrorCollection());
            SearchRequest searchRequest = searchRequestService.getFilter(serviceContext, new Long(filterId));
            if (searchRequest == null) {
                action.addErrorMessage(action.getText("report.error.no.filter"));
            }
        } catch (NumberFormatException nfe) {
            action.addError("filterId", action.getText("report.error.filter.id.not.a.number", filterId));
        }
    }
    @Override public boolean showReport() {

        ProjectManager projectManager = ComponentAccessor.getComponent(ProjectManager.class);
        ProjectActionSupport projectSupport = new ProjectActionSupport();
        Project currentProject = projectManager.getProjectObj(projectSupport.getSelectedProjectId());
        
        return currentProject.getKey().equals("REN1822");
    }       
}    