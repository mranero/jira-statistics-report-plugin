package com.atlassian.plugins.tutorial.jira.reports;
import com.atlassian.configurable.ValuesGenerator;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.fields.CustomField;
import java.util.Map;
import java.util.TreeMap;
import java.util.List;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.HashMap;
import org.apache.log4j.Logger;
public class ReportValuesGenerator implements ValuesGenerator <String> {
    private static final Logger log = Logger.getLogger(BudgetReport.class);
    public Map<String, String> getValues(Map userParams) {
        Map<String, String> customFieldsMap = new HashMap<String,String>();

        List<CustomField> allCustomFields =  ComponentAccessor.getCustomFieldManager().getCustomFieldObjects(); 
        for (CustomField customfield : allCustomFields) {
                if (customfield.getCustomFieldType().getKey().equals("com.atlassian.jira.plugin.system.customfieldtypes:float"))
                    customFieldsMap.put(customfield.getId().toString(),customfield.getName());
        }
        customFieldsMap = sortByValue(customFieldsMap);
        return customFieldsMap;
    }

    public static Map<String, String> sortByValue(final Map<String, String> wordCounts) {
        return wordCounts.entrySet()
                .stream()
                .sorted((Map.Entry.<String, String>comparingByValue()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }    
}